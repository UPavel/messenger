import createApp from './app';

const PORT = 3000;

createApp()
  .then((app) => {
    app.listen(PORT, () => console.log(`Server running on port: ${PORT}`));
  })
