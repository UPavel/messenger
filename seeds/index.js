import userSeeds from './user-seeds';
import messageSeeds from './message-seeds';
import mongooseConnect from './../mongoose-connect';

async function runUserSeeds() {
  // await User.remove();
  await userSeeds();
  console.log('user seed has been completed')
}

async function runMessageSeeds() {
  // await User.remove();
  await messageSeeds();
}

async function initSeeds() {
  const mongoConnection = await mongooseConnect('mongodb://localhost/messenger');

  try {
    await runUserSeeds();
    // await runMessageSeeds();
  } catch (e) {
    console.error(e);
  } finally {
    // mongoConnection.close();
  }
}

initSeeds()