import User from '../app/modules/user/models';
import faker from 'faker/locale/ru';

export default () => {
  const usersData = [];
  for(let i = 0; i < 200; i++) {
    usersData.push({
      displayName: faker.name.findName(),
      status: new Date()
    });
  }

  const users = usersData.map(userData => {
    return User.create(userData);
  });

  // console.log(users)

  return Promise.all(users);
}