/* import Message from '../app/modules/Message/models'; */
import messageService from '../app/modules/message/services'

export default async () => {
  const createMessage = async (countMessages) => {
    console.log(countMessages)
    if (countMessages === 100) {
      return
    }
    const authorId = '5b17f7ca1179752160ed5333';
    const text = `${countMessages}`;
    // const conversationId = '5b1a948c26a20b10d4c5e9ea';
    const conversationId = '5b197de1bcf0841c8c05d9b0';
    let message;
    try {
      message = await messageService.createMessage(text, authorId, conversationId);
    } catch (error) {
      console.log(error);
    }
    console.log(message)
    await new Promise((resolve, reject) => setTimeout(() => resolve(), 500));
    return createMessage(countMessages + 1)
  }
  
  return await createMessage(0);
}
