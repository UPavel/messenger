import socket from 'socket.io';

import messageService from './app/modules/message/services'
import conversationService from './app/modules/conversation/services'
import userService from './app/modules/user/services'

const onlineUsers = new Map();

export default (server) => {
  const io = socket(server);

  io.on('connection', (socket) => {
    socket.on('userOnline', async (userId) => {
      await userService.setUserStatus(userId, 'online');
      onlineUsers.set(userId, socket.id);
      const partnresId = await conversationService.getConversationsPartners(userId);
      const filteredPartneresId = partnresId.filter(partnerId => onlineUsers.has(partnerId));
      filteredPartneresId.forEach(partnerId =>
        socket.to(onlineUsers.get(partnerId)).emit('changedUserStatus', { userId, status: { type: 'online' } }));
    })

    socket.on('disconnect', async () => {
      let userId = null;
      onlineUsers.forEach((value, key) => {
        if (value === socket.id) {
          userId = key;
          onlineUsers.delete(key);
        }
      });
      const { status: lastSeen } = await userService.setUserStatus(userId, new Date());
      const partnresId = await conversationService.getConversationsPartners(userId);
      const filteredPartneresId = partnresId.filter(partnerId => onlineUsers.has(partnerId));
      filteredPartneresId.forEach((partnerId) => {
        socket.to(onlineUsers.get(partnerId)).emit('changedUserStatus', { userId, status: { type: 'offline', lastSeen } });
      });
    });

    socket.on('getConversations', async ({ userId }) => {
      const conversations = await conversationService.getConversations(userId);
      socket.emit('getConversations', conversations);
    })

    socket.on('newMessage', async (data) => {
      const { userId, partnerId, conversationId, text } = data;
      
      if (!conversationId) {
        const conversation = await conversationService.createConversation([userId, partnerId]);
        await messageService.createMessage(text, userId, conversation._id);
        const result = await conversationService.getConversation(conversation._id);
        if (onlineUsers.has(partnerId)) {
          const partnerSocket = onlineUsers.get(partnerId);
          socket.to(partnerSocket).emit('addConversation', result);
        }
        socket.emit('addConversation', result);
      } else {
        const { _id } = await messageService.createMessage(text, userId, conversationId);
        const message = await messageService.getMessage(_id);
        if (onlineUsers.has(partnerId)) {
          const partnerSocket = onlineUsers.get(partnerId);
          socket.to(partnerSocket).emit('newMessage', message);
        }
        socket.emit('newMessage', message);
      }
    });

    socket.on('getMessages', async ({ conversationId, skip }) => {
      const { messages, nextMessages } = await messageService.getMessages(conversationId, skip);
      socket.emit('getMessages', { conversationId, messages, nextMessages });
    });

    socket.on('getUsersByParams', async ({ userName }) => {
      const users = await userService.getUsersByParams(userName);
      socket.emit('getUsersByParams', { users });
    });

    socket.on('userTyping', async ({ authorId, partnerId, typing }) => {
      if (onlineUsers.has(partnerId)) {
        socket.to(onlineUsers.get(partnerId)).emit('parnterIsTyping', { partnerId: authorId, typing });
      };
    });

    socket.on('deleteMessages', async ({ conversationId, messageIds }) => {
      const result = await messageService.deleteMessages(messageIds)
      socket.emit('deleteMessages', { conversationId, messageIds });
    });

    socket.on('editMessage', async ({ conversationId, partnerId, messageId, text }) => {
      await messageService.editeMessage(messageId, text);
      if (onlineUsers.has(partnerId)) {
        const partnerSocket = onlineUsers.get(partnerId);
        socket.to(partnerSocket).emit('editMessage', { conversationId, messageId, text });
      }
      socket.emit('editMessage', { conversationId, messageId, text });
    });

    // dev
    socket.on('getUsers', async () => {
      const users = await userService.getUsers();
      socket.emit('getUsers', { users });
    });
  });
}