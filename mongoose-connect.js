import mongoose from 'mongoose';

mongoose.Promise = Promise;

export default async (mongoUri) => {
  if (!mongoUri) {
    throw Error('Mongo uri is undefined');
  }
  return mongoose.connect(mongoUri)
};
