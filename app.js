import http from 'http';
import Koa from 'koa';
import logger from 'koa-logger';
import session from 'koa-session';
/* import cookie from 'koa-cookie'; */
import cors from '@koa/cors';
import mongooseConnect from './mongoose-connect';
import socket from './socket';
import modules from './app/modules';

export default async () => {
  await mongooseConnect('mongodb://localhost/messenger');
  
  const app = new Koa();

  app.use(logger());

  app.use(cors({
    exposeHeaders: ['WWW-Authenticate', 'Server-Authorization'],
    maxAge: 5,
    credentials: true,
    allowMethods: ['GET', 'POST', 'DELETE', 'PUT'],
    allowHeaders: ['Cache-Control', 'Content-Type', 'Authorization', 'Accept'],
  }));

  app.keys = ['secret'];
  app.use(session({}, app));

  /* app.use(cookie());
  app.use(async function (ctx, next) {
    const cookies = ctx.cookie;
    console.log('cookies')
    console.log(cookies)
   await next();
  }); */

  app.use(modules);

  const server = http.createServer(app.callback());
  socket(server);

  return server;
}
