import mongoose, { Schema } from 'mongoose';

const userSchema = new Schema({
  providerName: {
    type: String,
    /* required: 'providerName is required', */
  },
  externalId: {
    type: String,
    /* required: 'externalId is required', */
  },
  profileUrl: {
    type: String,
    /* required: 'profileUrl is required', */
  },
  photoUrl: {
    type: String,
    /* required: 'photoUrl is required', */
  },
  displayName: {
    type: String,
    required: 'displayName is required',
  },
  gender: {
    type: String,
    /* enum: ['male', 'female'], */
  },
  birthday: {
    type: Date,
  },
  status: {
    type: String
  }
}, {
  timestamps: true,
});

export default mongoose.model('User', userSchema);
