import User from '../models';

const formatUser = user => {
  const {
    _id,
    displayName,
    status
  } = user;

  return {
    _id,
    userName: displayName,
    status
  }
}

export default {
  createUser(data) {
    return User.create(data);
  },

  async findOrCreate(userData) {
    if (!userData.photoUrl) {
      userData.photoUrl = 'https://ru.vuejs.org/images/logo.png';
    }
    const { providerName, externalId } = userData;
    const user = await User.findOne({ providerName, externalId });
    if (user === null) {
      const createdUser = await this.createUser(userData);
      return createdUser;
    } else {
      return user;
    }
  },

  async getUser(id) {
    const user = await User
      .findById(id)
      .lean();

    return user;
  },

  async getUsersByParams(userName) {
    const users = await User.find({ displayName: new RegExp(`.*${userName}.*`, 'i') }).lean();
    return users.map(user => formatUser(user));
  },

  // dev
  async getUsers() {
    const users = await User.find().limit(25);
    return users;
  },
  // dev

  async setUserStatus(userId, status) {
    const user = await User.findById(userId);
    user.set({ status });
    await user.save();
    return status;
  }  
}