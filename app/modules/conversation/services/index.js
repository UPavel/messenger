import Conversation from '../models';
import Message from '../../message/models';

const formatMessage = (message) => {
  const {
    _id,
    authorId: {
      _id: authorId,
      displayName: authorName
    }, 
    text,
    createdAt,
    isEdit
  } = message;
  return {
    _id,
    author: {
      authorId,
      authorName
    },
    text,
    createdAt,
    isEdit
  };
}

const formatConversation = (conversation) => {
  const { _id, members, messages } = conversation;
  return {
    conversationId: _id,
    members: members.map(({ _id: userId, displayName: userName, status }) => ({ userId, userName, status })),
    messages: messages.map(message => formatMessage(message))
  }
}

export default {
  createConversation(members) {
    return Conversation.create({ members });
  },

  async getConversations(userId) {
    const usersConversations = await Conversation
      .find({ members: { "$in" : [userId]} })
      .populate({
        path: 'members',
        model: 'User'
      })
      .lean();

    const conversationIds = usersConversations.map(({ _id }) => _id);

    const allMessages = await Message
      .find({ conversationId: { "$in": conversationIds } })
      .populate({
        path: 'authorId',
        model: 'User',
        select: 'displayName'
      })
      .lean();

    const groupedMessages = allMessages.reduce((acc, message) => {
      const conversationId = message.conversationId;
      if (!acc[conversationId]) {
        acc[conversationId] = [];
      }
      acc[conversationId].push(message);
      return acc;
    }, {});

    const sortedMessages = Object.keys(groupedMessages).reduce((acc, conversationId) => {
      return {
        ...acc,
        [conversationId]: groupedMessages[conversationId].sort((a, b) => b.createdAt - a.createdAt).slice(0, 1)
      }
    }, {});

    const result = Object.keys(sortedMessages)
      .map(conversationId => {
        const { members } = usersConversations.find(conversation => conversation._id.toString() === conversationId);
        return { _id: conversationId, members, messages: sortedMessages[conversationId] };
      })
      .map(conversation => formatConversation(conversation));

    return result;
  },

  /* async getConversation(conversationId) {
    const conversation = await Conversation
      .findById(conversationId)
      .populate({
        path: 'members',
        model: 'User',
        select: 'displayName',
      })
      .lean();

    return {
      conversationId: conversation._id,
      members: conversation.members
    };
  }, */
  
  async getConversation(conversationId) {
    const conversation = await Conversation
      .findById(conversationId)
      .populate({
        path: 'members',
        model: 'User'
      })
      .lean();

    const messages = await Message
      .find({ conversationId })
      .populate({
        path: 'authorId',
        model: 'User',
        select: 'displayName'
      })
      .sort({ createdAt: -1 })
      .limit(1)
      .lean();
    
      return {
        conversationId: conversation._id,
        members: conversation.members.map(({ _id: userId, displayName: userName, status }) => ({ userId, userName, status })),
        messages: messages.map(message => formatMessage(message))
      }
  },

  async getConversationsPartners(userId) {
    const conversations = await Conversation
      .find({ members: { "$in" : [userId]} })
      .select('members')
      .lean();

    const partnersId = conversations.map(({ members }) => {
      const partnerId = members.find(memberId => memberId.toString() !== userId);
      return partnerId.toString();
    });

    return partnersId;
  }
};
