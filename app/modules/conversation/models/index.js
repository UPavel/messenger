import mongoose, { Schema } from 'mongoose';

const conversationSchema = new Schema({
  members: {
    type: [mongoose.Schema.Types.ObjectId]    
  }
});

export default mongoose.model('Conversation', conversationSchema);
