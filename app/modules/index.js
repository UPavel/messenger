import Router from 'koa-router';
import auth, { passport } from './auth';

const router = new Router();
router.use(passport.initialize());
router.use(passport.session());
router.use(auth);

export default router.routes();