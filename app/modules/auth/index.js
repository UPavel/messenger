import { KoaPassport  } from 'koa-passport';
import { Strategy as VKontakteStrategy } from 'passport-vkontakte';
import Router from 'koa-router';
import User from '../user/models';
import UserService from '../user/services';

const passport = new KoaPassport();

passport.serializeUser((user, done) => {
  done(null, user._id);
});

passport.deserializeUser(async (id, done) => {
  const user = await User.findById(id).lean();
  done(null, user);
});

passport.use(new VKontakteStrategy(
  {
    clientID: 6124482,
    clientSecret: '',
    callbackURL: 'http://localhost:3000/api/auth/vkontakte/callback',
    profileFields: ['bdate'],
  },
  async (accessToken, refreshToken, params, profile, done) => {
    const {
      provider: providerName,
      id: externalId,
      profileUrl,
      displayName,
      gender,
      birthday,
      _json: {
        photo: photoUrl
      }
    } = profile;
    let userData = { providerName, externalId, profileUrl, displayName, gender, birthday, photoUrl };
    const user = await UserService.findOrCreate(userData);
    done(null, user);
  }
));

const router = new Router();

router.get('/api/auth/vkontakte/callback',
  passport.authenticate('vkontakte', {
    successRedirect: '/api/success',
    failureRedirect: '/api/fail',
  })
);

router.get('/auth/vkontakte', passport.authenticate('vkontakte'));

router.get('/api/success', (ctx) => {
  ctx.body = `
    <!DOCTYPE html>
    <html>
    <head>
        <script>
            (function () {
                window.opener.postMessage("auth", 'http://localhost:8080');
                window.close();
            })();
        </script>
    </head>
    <body>
    </body>
    </html>
    `;
});

router.get('/api/fail', (ctx) => {
  ctx.body = { auth: false };
});

router.get('/api/chechAuth', async (ctx) => {
  if (ctx.isAuthenticated()) {
    const userId = ctx.state.user._id;
    const user = await UserService.getUser(userId);
    ctx.body = { isAuth: true, data: { user } };
  } else {
    ctx.body = { isAuth: false };
  }
});

export {
  passport
}

export default router.routes();
