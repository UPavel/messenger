import Message from '../models';

const formatMessage = (message) => {
  const {
    _id,
    authorId: {
      _id: authorId,
      displayName: authorName
    }, 
    text,
    createdAt,
    isEdit
  } = message;
  return {
    _id,
    author: {
      authorId,
      authorName
    },
    text,
    createdAt,
    isEdit
  };
}

const formatMessageWithConversationId = (message) => {
  const {
    _id,
    authorId: {
      _id: authorId,
      displayName: authorName
    }, 
    text,
    createdAt,
    isEdit,
    conversationId
  } = message;
  return {
    _id,
    author: {
      authorId,
      authorName
    },
    text,
    createdAt,
    isEdit,
    conversationId
  };
}

export default {
  async createMessage(text, authorId, conversationId) {
    const { _id } = await Message.create({ text, authorId, conversationId });
    return await this.getMessage(_id);
  },

  // remove
  async getMessage(messageId) {
    const message = await Message
      .findById(messageId)
      .populate({
        path: 'authorId',
        model: 'User',
        select: 'displayName',
      })
      .lean();

    return formatMessageWithConversationId(message);
  },

  async getMessages(conversationId, skip, limit = 20) {
    const [messages, countMessages] = await Promise.all([
      Message
        .find({ conversationId })
        .populate({
          path: 'authorId',
          model: 'User',
          select: 'displayName',
          as: 'author',
        })
        .sort({ createdAt: -1 })
        .skip(skip)
        .limit(limit)
        .lean(),
      Message
        .find({ conversationId })
        .count()
    ]);

    const mappedMessages = messages.map(message => formatMessage(message));
    const nextMessages = (skip + messages.length) < countMessages;
    
    return {
      messages: mappedMessages,
      nextMessages
    };
  },

  deleteMessages(messageIds) {
    return Message.remove({ _id: { "$in": messageIds } });
  },

  async editeMessage(messageId, newText) {
    const message = await Message.findById(messageId);
    message.set({ text: newText, isEdit: true });
    await message.save();
    return;
  }
}
