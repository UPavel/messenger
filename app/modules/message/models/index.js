import mongoose, { Schema } from 'mongoose';

const messageSchema = new Schema({
  text: {
    type: String,
    required: 'text is required'
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  authorId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: 'authorId is required'
  },
  reply: {
    type: Boolean,
    default: false
  },
  isEdit: {
    type: Boolean,
    default: false
  },
  isRead: {
    type: Boolean,
    required: 'isRead is required',
    default: false
  },
  unread: {
    type: [mongoose.Schema.Types.ObjectId]
  },
  conversationId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Conversation',
    required: 'conversationId is required'
  }
});

export default mongoose.model('Message', messageSchema);
